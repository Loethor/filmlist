

class Switcher(object):
    def __init__(self, list_of_statements):
        self.switcher = {}
        
        for count,element in enumerate(list_of_statements, 1):
            self.switcher[count] = element
    
        # print(self.switcher)
    def get_dict(self):
        return self.switcher
            