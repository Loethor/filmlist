import time
import random
import os

from classes.Switcher import Switcher

class FilmList(object):
    
    def __init__(self, listfile, seenfile):
        self.listfile = listfile
        self.seenfile = seenfile
        self.film_list = []
        self.create_and_renew_list()
        
        switch = Switcher(["Show", "Add", "Remove", "Suggest", "Seen", "Help", "Exit"])
        self.options = switch.get_dict()
        # print(options)
        
        # random seed
        random.seed(a=time.time())
        
        self.switch_loop()
        

        
        
    def create_and_renew_list(self):
        film_list = []
        if os.path.exists(self.listfile) is False:
            with open(self.listfile, 'a+') as f:
                pass
            
            # and os.stat(self.listfile).st_size == 0:
        # elif os.stat(self.listfile).st_size != 0:
        with open(self.listfile, 'r') as f:
            for line in f:
                film_list.append(line.strip())
                    
        self.film_list = film_list
        
    def add_to_list(self, newfilm):
        with open(self.listfile,'a+' ) as f:
            f.write(newfilm + "\n")
        self.create_and_renew_list()
        
    def update_seen(self, newfilm):
        with open(self.seenfile,'a+' ) as f:
            f.write(newfilm + "\n")
        
        
        
    def get_list(self):
        if not self.film_list:
            return []
        else:
            return self.film_list
    
    def choose_from_list(self):
        return random.choice(self.film_list)
        
        
    def switch_loop(self):
        while True:
            print("Please choose a Number:")
            print("1) Show")
            print("2) Add")
            print("3) Remove")
            print("4) Suggest")
            print("5) Seen")
            print("6) Help")
            print("7) Exit")
        
            user_input = input()
            try:
                msg = self.options.get(int(user_input))
            except:
                print("Nice try :-)")
                continue
            
            if msg == "Add":
                print("Please type the name of the film.")
                film = input()
                self.add_to_list(film)
                print("'{}' was added to the list.".format(film))
                print("")
            elif msg == "Show":
                my_list = self.get_list()
                if  my_list:
                    print("")
                    for count,my_film in enumerate(my_list,1):
                        print('{}. {}'.format(count,my_film))
                    print("")
                    input("Press Enter to continue...")
                else:
                    print("The list is empty! Add some films :-)")
                    print("")
            elif msg == "Remove":
                pass
            
            elif msg == "Suggest":
                my_list = self.get_list()
                if  my_list:
                    print("")
                    print("I thinkg you should watch {}".format(random.choice(my_list)))
                    print("")
                    input("Press Enter to continue...")
                else:
                    print("The list is empty! Add some films :-)")  
                
            elif msg == "Seen":
                pass    
              
            elif msg == "Help":
                print("""
There are the options you can chose.
1) Show: Shows the full list of films.
2) Add: Adds a film into the list. 
3) Remove: Removes a film from the list. 
4) Suggest: Suggests a film from the list. 
5) Seen: Moves a film from the list to seen list. 
6) Help: Shows help. 
7) Exit: Exits program.
""")
                input("Press Enter to continue...")
                
            elif msg == "Exit":
                print("Good bye!")
                break
            
            else:
                print("")
                print("Sorry, That didn't work. Try Again.")
                print("")
        