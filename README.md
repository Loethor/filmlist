# FilmList

FilmList is a Python module to manage your film lists.

## Features

 - `Show`: Shows the full list of films.
 - `Add`: Adds a film into the list. 
 - `Remove`: Removes a film from the list. 
 - `Suggest`: Suggests a film from the list. 
 - `Seen`: Moves a film from the list to seen list. 
 - `Help`: Shows help. 
 - `Exit`: Exits program.



## Usage

```python
from classes.FilmList import FilmList

film_list = FilmList("filmlist.txt","seenlist.txt")
```

Where `filmlist.txt` is a file where the films are stored and `seenlist.txt` is a file where the seen films are stored.

